컬렉터
    스트림 요소를 어떤 식으로 도출할지
    컬렉터를 실행하면 reduce 연산이 수행되고, 결과가 모아짐

    종류:
        1. 하나의 값으로 리듀스, 요약
        2. 그룹화
        3. 분할

1. 하나의 값으로 리듀스, 요약
    Collectors.counting()
        갯수 세기
        menu.stream().count() 로 간소화할 수도 있다

    Collectors.minBy(Comparator<T>)
    Collectors.maxBy(Comparator<T>)
        최대/최소값 찾기

    Collectors.summingInt
    Collectors.summingLong
    Collectors.summingDouble
    Collectors.average...
        자료형 특화
    
    Collectors.summarizingInt
    Collectors.summarizingLong
    Collectors.summarizingDouble
        count,
        sum,
        min,
        max,
        average,
        max
        를 모두 가지고 있는 IntSummaryStatistics 가 반환됨
    
    Collectors.joining
        Stream<String> 을 합치는 메소드
        T.toString 이 있다면 Stream<T> 에 사용할 수도 있음
        splitter 넣을 수도 있음
    
    Collecetors.reducing(T, (S) -> T, (T, T) -> T)
        범용
        파라미터
            초기값, stream이 비어있는 경우 반환값
            스트림에서 결과값으 변환하는 메소드
            두개의 결과값을 합치는 메소드
    
2. 다수준 그룹화, 어려움
    Collectors.groupingBy((T) -> S)
        Map<S, T> 로 만든다

        메소드 참조로 만들어도 되고,
        규칙이 복잡한 경우엔 람다식으로 만들어도 된다

    Map<Dish.Type, List<Dish>> caloricDishesByType = 
        menu.stream()
            .collect(
                groupingBy(
                    Dish::getType,
                    filtering(dish -> dish.getCalories() > 500, toList())
                )
            );
    
        Dish.Type 을 사용해 스트림으로 만들어냈음
        Type 에 해당하는 값이 없어도(FISH) 빈 리스트가 반환됨

        {OTHER=[a,b,c], MEAT=[d,e,f], FISH=[]}
    
    Map<Dish.Type, List<String>> dishNamesByType =
        menu.stream()
            .collect(
                groupingBy(
                    Dish::getType,
                    mapping(Dish::getName, toList())
                )
            );
    
    Map<Dish.Type, Set<String>> dishNamesByType = 
        menu.stream()
            .collect(
                groupipngBy(
                    Dish::getType,
                    flatMapping(
                        dish -> dishTags.get(
                            dish.getName()
                        ).stream()
                    )
                )
            )

    Collectors.collectingAndThen(C -> T)
        적용할 컬렉터와 변환함수를 인수로 받아 다른 컬렉터를 반환

3. 분할
    Collectors.partitioningBy(Predicate, Collector)

    predicate 로 boolean 에 따라 Map<Boolean, S> 를 리턴함

4. Collector 인터페이스
    인터페이스를 구현해 커스텀 Collector 를 만들 수 있음

    public interface Collector<T,A,R> {
        Supplier<A> supplier();                 //비어있는 결과 생성
        BiConsumer<A, T> accumulator();         //리듀싱 연산 수행, 항목을 결과에 추가
        Function<A, R> finisher();              //끝낸 후 수행할 연산, 결과가 이미 최종결과면 항등함수 Function.identity() 반환
        BinaryOperator<A> combiner();           //서로 다른 서브파트를 병렬로 처리할 때 합칠 방법
        Set<Charasteristics> characteristics(); //최적화 위한 힌트 특성 집합
    }

    T: 수집될 항목
    A: 누적자, 중간결과 누적
    R: 결과 형식

    Characteristics: 병렬로 리듀스 할지, 어떤 최적화 쓸지... 등등의 최적화 힌트, enum 임
        UNORDERED: 방문순서, 누적순서 영향 없음
        CONCURRENT: 다중 스레드에서 accumulator 함수 동시에 호출 가능, 병렬 리듀싱 가능
        IDENTIFY_FINISH: finish 는 identity 이므로 finish 연산을 생략해도 된다, 리듀싱의 결과가 바로 최종결과임
    

    커스텀 콜렉터로 takeWhile 도 만들 수 있음
